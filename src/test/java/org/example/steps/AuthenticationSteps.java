package org.example.steps;

import net.thucydides.core.pages.PageObject;
import org.example.models.Person;
import org.example.pageobjects.LoginPage;
import org.example.pageobjects.RegisterPage;

import static org.hamcrest.MatcherAssert.assertThat;

public class AuthenticationSteps extends PageObject {
    RegisterPage registerPage;
    LoginPage loginPage;

    public void testPrepare(String initPage) {
        if (initPage.equals("Register")) {
            registerPage.open();
        } else {
            loginPage.open();
        }
    }

    public void fillRegisterForm(Person person) {
        registerPage.fillForm(person);
        registerPage.signUp();
    }

    public void validateCorrectRegister(Person firstPerson) {
        assertThat(registerPage.getTxtSuccessRegister().getText(), containsText("You have been registered as "
                .concat(firstPerson.getFirstName())
                .concat(" ")
                .concat(firstPerson.getLastName())
                .concat("!")
        ));
    }

    public void fillLoginForm(Person person) {
        loginPage.fillForm(person);
        loginPage.signIn();
    }

    public void validateIncorrectLogin() {
        assertThat(loginPage.getTxtInfoLogin().getText(), containsText("Please provide correct login data."));
    }

}
