package org.example.models;

public class Person {
    private String firstName;
    private String lastName;
    private String email;
    private String pass;
    private String confirmPass;
    private String phone;
    private String wantSuscribe;

    public Person(String firstName, String lastName, String email, String pass, String confirmPass, String phone, String wantSuscribe) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.pass = pass;
        this.confirmPass = confirmPass;
        this.phone = phone;
        this.wantSuscribe = wantSuscribe;
    }

    public Person(String firstName, String lastName, String email, String pass, String phone, String wantSuscribe) {
        this.email = email;
        this.pass = pass;
    }

    public Person(String email, String pass) {
        this.email = email;
        this.pass = pass;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPhone() {
        return phone;
    }

    public String getWantSuscribe() {
        return wantSuscribe;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

}
