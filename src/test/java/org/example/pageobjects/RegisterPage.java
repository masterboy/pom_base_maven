package org.example.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.example.models.Person;

@DefaultUrl("https://mdbootstrap.com/docs/jquery/admin/auth/register/")
public class RegisterPage extends PageObject {

    @FindBy(id = "register-first-name")
    public WebElementFacade txtFirstName;

    @FindBy(id = "register-last-name")
    public WebElementFacade txtLastName;

    @FindBy(id = "register-email")
    public WebElementFacade txtEmail;

    @FindBy(id = "register-password")
    public WebElementFacade txtPass;

    @FindBy(id = "register-password-confirm")
    public WebElementFacade txtConfirmPass;

    @FindBy(xpath = "//*[@id='register-phone']")
    public WebElementFacade txtPhone;

    @FindBy(xpath = "//*[@class=\"form-check-label small text-uppercase card-link-secondary\"]")
    public WebElementFacade chkBoxSuscribe;

    @FindBy(xpath = "//button[contains(text(), 'Sign Up')]")
    public WebElementFacade btnSignUp;

    @FindBy(xpath = "//p[contains(@class, 'logged-user-message')]")
    public WebElementFacade txtSuccessRegister;

    public WebElementFacade getTxtSuccessRegister() {
        return txtSuccessRegister;
    }

    public void fillForm(Person person) {
        txtFirstName.sendKeys(person.getFirstName());
        txtLastName.sendKeys(person.getLastName());
        txtEmail.sendKeys(person.getEmail());
        txtPass.sendKeys(person.getPass());
        txtConfirmPass.sendKeys(person.getConfirmPass());
        txtPhone.sendKeys(person.getPhone());
        if (person.getWantSuscribe().equals("true")) {
            chkBoxSuscribe.click();
        }
    }

    public void signUp() {
        btnSignUp.click();
    }
}
