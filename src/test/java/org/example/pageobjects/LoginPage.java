package org.example.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.example.models.Person;

@DefaultUrl("https://mdbootstrap.com/docs/jquery/admin/auth/login/")
public class LoginPage extends PageObject {


    @FindBy(id = "login-email")
    public WebElementFacade txtEmail;

    @FindBy(id = "login-password")
    public WebElementFacade txtPassword;

    @FindBy(xpath = "//*[@class=\"form-check-label small text-uppercase card-link-secondary\"]")
    public WebElementFacade chkBoxRememberMe;

    @FindBy(xpath = "//button[contains(text(), 'Sign in')]")
    public WebElementFacade btnSignIn;

    @FindBy(xpath = "(//div[contains(@class, 'modal-body')])[2]")
    public WebElementFacade getTxtInfoLogin;

    public WebElementFacade getTxtInfoLogin() {
        return getTxtInfoLogin;
    }


    public void fillForm(Person person) {
        txtEmail.sendKeys(person.getEmail());
        txtPassword.sendKeys(person.getPass());
    }

    public void signIn() {
        btnSignIn.click();
    }

}
