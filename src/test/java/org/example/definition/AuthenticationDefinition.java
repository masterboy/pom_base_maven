package org.example.definition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.example.models.Person;
import org.example.steps.AuthenticationSteps;

import java.util.List;

public class AuthenticationDefinition {
    Person firstPerson;

    @Steps
    AuthenticationSteps authenticationSteps;

    @Given("^Yo estoy en la pagina de registro de mdbootstrap$")
    public void yoEstoyEnLaPaginaDeRegistroDeMdbootstrap() {
        authenticationSteps.testPrepare("Register");
    }

    @When("^lleno formulario de registro$")
    public void llenoFormularioDeRegistro(List<Person> persons) {
        firstPerson = persons.get(0);
        authenticationSteps.fillRegisterForm(firstPerson);
    }

    @Given("^Yo estoy en la pagina de login de mdbootstrap$")
    public void yoEstoyEnLaPaginaDeLoginDeMdbootstrap() {
        authenticationSteps.testPrepare("Login");
    }

    @When("^lleno formulario de login con user ([^\"]*) y pass ([^\"]*)$")
    public void llenoFormularioDeLoginConUserYPass(String user, String pass) {
        Person newPerson = new Person(user, pass);
        authenticationSteps.fillLoginForm(newPerson);
    }

    @Then("^Verifico login fallido$")
    public void verificoLoginFallido() {
        authenticationSteps.validateIncorrectLogin();
    }

    @Then("^Verifico registro exitoso$")
    public void verificoRegistroExitoso() {
        authenticationSteps.validateCorrectRegister(firstPerson);
        
    }
}
