package org.example.utils;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.MalformedURLException;

public class RunDriverTest {

    public static void main(String[] args) throws MalformedURLException {
        System.out.println("prueba ejecucion driver");
        //System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver");
        //WebDriver driver = new FirefoxDriver();
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://chromedriver.chromium.org/getting-started");
        driver.quit();
    }
}