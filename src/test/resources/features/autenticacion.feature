@autenticacion
Feature: Funcionalidad de autenticacion
  consta de una pagina de registra y otra de login.

  @registro   @registro1
  Scenario: Registro exitoso en mdbootstrap con suscripcion
    Given Yo estoy en la pagina de registro de mdbootstrap
    When lleno formulario de registro
      | firstName | lastName | email             | pass   | confirmPass | phone         | wantSuscribe |
      | Ricardo   | Ospina   | rlospina@yahoo.es | 123456 | 123456      | +573017839876 | true         |
    Then Verifico registro exitoso

  @registro @registro2
  Scenario: Registro exitoso en mdbootstrap sin suscripcion
    Given Yo estoy en la pagina de registro de mdbootstrap
    When lleno formulario de registro
      | firstName | lastName | email        | pass   | confirmPass | phone         | wantSuscribe |
      | Alonso    | Mejia    | alo@yahoo.es | 678910 | 678910      | +573128956745 | false        |
    Then Verifico registro exitoso

  @login1
  Scenario: Login fallido en mdbootstrap
    Given Yo estoy en la pagina de login de mdbootstrap
    When lleno formulario de login con user ricardo y pass 1234567
    Then Verifico login fallido


  #palabras reservadas Gherkins
#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template